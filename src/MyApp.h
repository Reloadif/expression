#pragma once
#include <AppCore/AppCore.h>
#include "mExpression.h"
#include "JavaScriptCore/JSStringRef.h"


using namespace ultralight;

class MyApp : public AppListener,
              public WindowListener,
              public LoadListener,
              public ViewListener {
public:
  MyApp();

  virtual ~MyApp();

  // Start the run loop.
  virtual void Run();

  // This is called continuously from the app's main loop.
  virtual void OnUpdate() override;

  // This is called when the window is closing.
  virtual void OnClose() override;

  // This is called whenever the window resizes.
  virtual void OnResize(uint32_t width, uint32_t height) override;

  // This is called when the page finishes a load in one of its frames.
  virtual void OnFinishLoading(ultralight::View* caller,
                               uint64_t frame_id,
                               bool is_main_frame,
                               const String& url) override;

  // This is called when the DOM has loaded in one of its frames.
  virtual void OnDOMReady(ultralight::View* caller,
                          uint64_t frame_id,
                          bool is_main_frame,
                          const String& url) override;

  // This is called when the page requests to change the Cursor.
  virtual void OnChangeCursor(ultralight::View* caller,
    Cursor cursor) override;

  virtual void OnChangeTitle(ultralight::View* caller,
    const String& title) override;

  JSValue GetMessage(const JSObject& thisObject, const JSArgs& args) {
     const JSValue* stringJS = args.data();
     char buff[2048];
     JSStringGetUTF8CString((JSString)stringJS[0], buff, 2048);
     std::string stringC(buff);

     mExpression myExp(stringC);

     String answer = "";
     try {
         answer += "<p>";
         answer += JSString(JSStringCreateWithUTF8CString(myExp.checkBrackets().c_str()));
         answer += "</p>";
         answer += "<p>";
         answer += JSString(JSStringCreateWithUTF8CString(myExp.createPostfix().c_str()));
         answer += "</p>";

         if (myExp.getIsInt()) {
             JSValue number = myExp.calculatedPostfixDouble();
             answer += "<p>";
             answer += number;
             answer += "</p>";
         }
         else {
             JSValue number = myExp.calculatedPostfixDouble();
             answer += "<p>";
             answer += number;
             answer += "</p>";
         }
     }
     catch (std::exception e) {
         answer += "<p>";
         if (*e.what() == *(const char*)"Stack is empty!") {
             answer += "I said don't use increment!";
         }
         else {
             answer += e.what();
         }
         answer += "</p>";
     }

     return answer;
  }


protected:
  RefPtr<App> app_;
  RefPtr<Window> window_;
  RefPtr<Overlay> overlay_;
};
