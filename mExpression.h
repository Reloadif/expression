#pragma once

#include <iostream>
#include <string>
// #include <exception> 

#include "TStack.h"


class mExpression {
	std::string expression;

	bool isCalculated;
	bool isCorrectBrackets;
	bool isInt;
public:
	mExpression(const std::string _expression);

	const bool getIsInt() const;

	const std::string getExpression() const;

	const std::string checkBrackets();

	const std::string createPostfix();

	double calculatedPostfixInt();
	double calculatedPostfixDouble();

};
